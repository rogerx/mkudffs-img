
# create a sparse image file
# available capacity of optical media, size in bytes
truncate --size=25025314816 /tmp2/photos-2022.udf

# format the image file
mkudffs --utf8 --blocksize=2048 --lvid="photos-2022" /tmp2/photos-2022.udf

# mount the image file using the kernel loop file system driver
sudo mount -t udf -o uid=forget,gid=forget,dmode=777,mode=777 /tmp2/photos-2022.udf /mnt/tmp

# copy over files to the mounted loop file system
time rsync -ax --verbose ./ /mnt/tmp/

# unmount the loop file system
sudo umount /mnt/tmp/

# image is ready for writing to optical media
cdrecord -prcap dev=3,0,0

cdrecord dev=3,0,0 driveropts=burnfree /tmp2/photos-2022.udf

