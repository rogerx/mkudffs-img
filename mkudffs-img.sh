#!/bin/bash
# AUTHOR: Roger Zauner (rdz), rogerx [dot] oss [AT] gmail [dot] com
# LICENCE: GPL-3 2023, Roger Zauner

# mkudffs-img.sh -o [image file] -v [volume id] ./files


# INSTRUCTIONS
# Either create "/tmp2" or change the location for the UDF image
# destination.
#
# Either create /mnt/tmp for loopfs mounting, or change
# _mount_dir="/mnt/tmp" to something else.
#
# Ensure you have the required non-bash related commands listed below
# installed.
#
# The following will is a basic incanatation:
# $ mkudffs-img.sh -l photos-2022 -o photos-2022.img /home/roger/photos/2022
#
# Other variables for customizing are listed below, near the top of the script.


# QUIRKS
# When specifying directories, ensure there is no trailing slash.
# Reason, rsync will attempt to modify the top dir, not resulting with
# return zero.
#
# I commonly use DVD+R/RW plus media, so the script defaults to size of
# DVD plus media.  If you have DVD-R/RW minus media, will need to modify
# the blocksize of the UDF image for 4.7GB media.  Also, I have no 8GB
# or DVD dual layer media.


# NON-BASH REQUIRED COMMANDS
# du - check total size of specified files/folders
# truncate - creates a sparse file
# mkudffs - creates/modifies UDF file images
# mount - mount loop image/file system
# rsync - copies files to mounted file system
# umount - unmounts file system


# TODO: add checks for getopts usage and non-builtin commands?
# TODO: check for already mounted /mnt/tmp!
# TODO: check for adequate space on _image_dir!
# TODO: add dry-run?
# TODO: check/use for $HOME/.mkudffsrc, although none natively supported
# by mkudffs.
# TODO: once completed, dump udfinfo results?

set -o nounset          # Treat unset variables as an error

_debug_val="${DEBUG:=0}"    # 0 = no debug output, 1 = show debug output
                            # Or enable debug with:
                            # DEBUG=1 mkudffs-img.sh


# VARIABLES

# output image dir and output image file name
# this should not be memory mounted tmpfs /tmp dir!
# needs free space, size of optical media (eg. ~50GB)
# mkdir /tmp2 && chmod a+rwX -R /tmp2
# TODO: test available free space on _image_dir!
# CHANGE THIS TO YOUR UDF IMAGE OUTPUT/SAVE DIR
# default /tmp2
declare _image_dir="/tmp2"

# default UDF image name
declare _image_name="out.udf"

# rsync options
# FIXME: STILL WANTS TO WRITE TIMES TO TOP /. DIR!
declare -r _rsync_options="-ax"

# typical 12cm optical disc capacity
# http://www.osta.org/technology/dvdqa/dvdqa6.htm
#
# 700MB CD-R 737280000 bytes
# 4.7GB DVD-R 4700000000 bytes
# 4.7GB DVD-RW 4700000000 bytes
# 4.7GB DVD+R 4700372992 bytes
# 4.7GB DVD+RW 4700372992 bytes
# 25GB BD-R 25025314816 bytes not formatted
# 50GB BD-R 50050629632 bytes not formatted
#
# the following are set for CD 700MB, DVD+R/+RW 4.7GB
# need to modify if using DVD-R/-RW, other sized CD!
declare -g -i -r _cdr=737280000
declare -g -i -r _dvd=4700372992
declare -g -i -r _bdr25=25025314816
declare -g -i -r _bdr50=50050629632
declare -g -i -r _bdr100=100103356416

# mkudffs
# set to mkudffs defaults
declare _mkudffs_contact=""
declare _mkudffs_lvid="LinuxUDF"
declare _mkudffs_org=""
declare _mkudffs_owner=""
declare _mkudffs_vid="${_mkudffs_lvid}"
declare _mkudffs_vsid="LinuxUDF"

# mount
# CHANGE TO YOUR MOUNT POINT FOR LOOPFS UDF IMAGE FILE
# default /mnt/tmp
declare _mount_dir="/mnt/tmp"
# mount now explicit requires UID/GID defined before using forget?
declare _mount_options="-o uid=$UID,gid=$UID,uid=forget,gid=forget,dmode=777,mode=777"



# FUNCTIONS

debug()         # ifdef style debug
{
    [ ${_debug_val} -ne 0 ] && "$@"
    # sample usage:
    # debug echo "variable  my_var="${my_var}
    # debug echo "DEBUG: return value: ${?}"

}

echo()          # replace echo with safe drop-in printf
{
    local IFS=' '; printf '%s\n' "$*";
}

# TODO: add either -p (plus) or -m (minus) for either dvd+/dvd- 4.7G size?
# currently defaults to dvd+r/+rw byte capacities
get_options()   # Parse command line arg/option/param
{
    debug echo "  DEBUG: In function get_options"

    # help options
    _usage="Usage: $0 -h [help] -l [volume label] -o [image file] -v [verbose] ./files"

    declare -i OPTIND=1
    declare _option=""

    # NOTE: colon indicates option requires an argument.
    # NOTE: colon prefixed string causes getopts to run in silent mode
    declare -r OPTSTRING="hl:o:v"
    debug echo "  DEBUG: var OPTSTRING = ${OPTSTRING}"

    while getopts "${OPTSTRING}" _option; do
        debug echo "  DEBUG: var Option Index=${OPTIND}" # NOTE: OPTARG NOT USED YET!
        # NOTE: When adding options, be sure to add them to $OPTSTRING above!
        case ${_option} in
            h) echo "${_usage}"
                exit 0
                ;;

            l) _mkudffs_lvid=${OPTARG}
                _mkudffs_vid=${OPTARG}
                ;;

            o) _image_name=${OPTARG}
                ;;
            v) _debug_val=1
                ;;

            \?) echo "${_usage}"
                    echo "Option not recognized."
                    exit 1
                    ;;

            *) echo "${_usage}"
                echo "Option -$OPTARG requires an argument."
                exit 1
                ;;
        esac
    done

    # For additional arguments not using an option (eg. files & folders to
    # record)
    # remove already processed getopts parameters.
    shift $((${OPTIND}-1))
    declare _file=""
    declare -a -g _files=()

    # loop over remaining arguments
    # $@ = script's command line args, what's left after shift $OPTIND
    # $# = total number of args, what's left after shift $OPTIND
    # https://unix.stackexchange.com/questions/314032/how-to-use-arguments-like-1-2-in-a-for-loop
    i=0
    # next statement safe for most POSIX and modern bash shells,
    # implies implies: for arg in "$@";
    for _file do
        debug echo "  DEBUG: var FILE ${i}: ${_file}"
        _files[$i]="${_file}"
        
        i=$((i + 1))
    done

    debug echo "  DEBUG: var _file: ${_file}"
    debug echo "  DEBUG: var _files: ${_files[@]}"

    debug echo "  DEBUG: var _usage=${_usage}"
    debug echo "  DEBUG: var _image_name=${_image_name}, var _mkudffs_lvid=${_mkudffs_lvid}, var _debug_val=${_debug_val}"

    debug echo "  DEBUG: Exit function get_options"
}

check_vars()
{
    debug echo "  DEBUG: In function check_vars"

    if [ ! -d "${_image_dir}" ]; then
        echo "UDF image destination directory is not set!"
        echo "Edit mkudffs-img.sh and check _image_dir variable."
        exit 1
    fi

    if [ ! -w "${_image_dir}" ]; then
        echo "UDF image destination directory is not writable!"
        echo "Edit mkudffs-img.sh and check _image_dir variable."
        exit 1
    fi
    
    debug echo "  DEBUG: Exit function check_vars"
}

check_size()    # check size of list/array of files
{
    debug echo "  DEBUG: In function chech_size"

    debug echo "  DEBUG: var _files = ${_files[@]}"

    debug printf "  " && declare -p _files

    declare -g -i _size_bytes
    declare -g _size_human


    _size_bytes=$(du --bytes --total "${_files[@]}" | tail --lines=1 | cut --fields=1)
    debug echo "  DEBUG: var _size_bytes = ${_size_bytes}"

    _size_human=$(du --bytes --total --human-readable "${_files[@]}" | tail --lines=1 | cut --fields=1)
    debug echo "  DEBUG: var _size_human = ${_size_human}"

    # not tested du optional
    #find "${_files[@]}" -print0 | du -ch --files0-from=- --total -s|tail -1

    echo
    echo "Size ${_size_human}"

    debug echo "  DEBUG: Exit function check_size"
}

truncate_img()     # create/truncate image file
{
    debug echo "  DEBUG: In function truncate_img"

    declare _size_optical="not defined"

    if (($_size_bytes <= $_cdr));
    then
        _size_img=$_cdr
        _size_optical="700MB"

    elif (($_size_bytes > $_cdr)) && (($_size_bytes <= $_dvd))
    then
        _size_img=$_dvd
        _size_optical="4.7GB"

    elif (($_size_bytes > $_dvd)) && (($_size_bytes <= $_bdr25))
    then
        _size_img=$_bdr25
        _size_optical="25GB"

    elif (($_size_bytes > $_bdr25)) && (($_size_bytes <= $_bdr50))
    then
        _size_img=$_bdr50
        _size_optical="50GB"

    elif (($_size_bytes > $_bdr50)) && (($_size_bytes <= $_bdr100))
    then
        _size_img=$_bdr100
        _size_optical="100GB"

    elif (($_size_bytes > $_bdr100))
    then
        echo "media size not supported, size = "$_size_bytes
        exit 1

    fi

    debug echo "  DEBUG: var _size_bytes = ${_size_bytes} bytes"
    debug echo "  DEBUG: var _size_img = ${_size_img}"

    echo "Size of optical media required: ${_size_optical}"

    read -p "Size OK? [y/n] " answer
    echo
    
    if [ "${answer,,}" != "y" ]; then
        echo "Answer other than yes.  Exiting."
        exit 0
    fi

    debug echo "  DEBUG: truncate --size=${_size_img} ${_image_dir}/${_image_name}"
    truncate --size=${_size_img} "${_image_dir}"/"${_image_name}"

    if [ $? -ne 0 ]; then
        echo "truncate ${_image_dir}/${_image_name} failed."
        exit 1
    fi
    
    debug echo "  DEBUG: Exit function truncate_img"
}

format_img()
{
    debug echo "  DEBUG: In function format_img"

    debug echo "  DEBUG: mkudffs --utf8 --blocksize=2048 --contact=${_mkudffs_contact} --lvid=${_mkudffs_lvid} --organization=${_mkudffs_org} --owner=${_mkudffs_owner} --vid=${_mkudffs_vid} --vsid=${_mkudffs_vsid} ${_image_dir}/${_image_name}"
    
    mkudffs --utf8 --blocksize=2048 --contact="${_mkudffs_contact}" --lvid="${_mkudffs_lvid}" --organization="${_mkudffs_org}" --owner="${_mkudffs_owner}" --vid="${_mkudffs_vid}" --vsid="${_mkudffs_vsid}" "${_image_dir}"/"${_image_name}"
    
    if [ $? -ne 0 ]; then
        echo "mkudffs ${_image_dir}/${_image_name} failed."
        exit 1
    fi

    debug echo "  DEBUG: Exit function format_img"
}

mount_img()          # mount created UDF image to $_mount_dir
{
    debug echo "  DEBUG: In function mount_img"
    
    debug echo "  DEBUG: sudo mount -t udf ${_mount_options} ${_image_dir}/${_image_name} ${_mount_dir}"

    # do not double quote mount options, mount "bad usage" error.
    sudo mount -t udf ${_mount_options} "${_image_dir}"/"${_image_name}" "${_mount_dir}"
    
    if [ $? -ne 0 ]; then
        echo "sudo mount ${_image_dir}/${_image_name} failed."
        exit 1
    fi
    
    debug echo "  DEBUG: Exit function mount_img"
}

copy_files()
{
    debug echo "  DEBUG: In function copy_files"
    
    unset _file

    # for each file in _files, copy to mounted dir
    for _file in "${_files[@]}"; do
        # check source directory has no ending slash, prevent modifying destination top dir times!
        # optional: echo "${_file:0-1}"
        # optional: echo -n $_file | tail -c 1
        declare _last_char
        _last_char="${_file: -1}"
        if [[ $_last_char = "/" ]]; then
            echo "ERROR: check source dir endings terminate without a forward slash."
            echo "Terminate source dir endings using either a file name or a '*' glob character."
            echo "eg. rsync -ax /some/dir /mnt/tmp/"
            echo "eg. rsync -ax /some/dir/* /mnt/tmp/"

            unmount_img

            remove_img

            exit 1
        fi

        debug echo "  DEBUG: rsync ${_rsync_options} ${_file} ${_mount_dir}"

        rsync ${_rsync_options} "${_file}" "${_mount_dir}"
        
        if [ $? -ne 0 ]; then
            echo "copy/rsync failed."
            echo
            echo "If \"failed to set times on "${_mount_dir}/.": Operation not permitted\", then"
            echo "check source dir endings terminate without a forward slash."
            echo "Terminate source dir endings using either a file name or a '*' glob character."
            echo "eg. rsync -ax /some/dir /mnt/tmp/"
            echo "eg. rsync -ax /some/dir/* /mnt/tmp/"

            unmount_img

            remove_img

            exit 1
        fi
    done

    sync
    
    debug echo "  DEBUG: Exit function copy_files"
}

unmount_img()
{
    debug echo "  DEBUG: In function unmount_img"
    
    debug echo "  DEBUG: sudo umount ${_image_dir}/${_image_name}"
    
    sudo umount "${_image_dir}"/"${_image_name}"
    
    if [ $? -ne 0 ]; then
        echo "sudo umount ${_image_dir}/${_image_name} failed."
        exit 1
    fi
    
    debug echo "  DEBUG: Exit function unmount_img"
}

remove_img()
{
    debug echo "  DEBUG: In function remove_img"

    echo "Removing UDF image file due to error during writing."

    rm -i --preserve-root=all "${_image_dir}"/"${_image_name}"
    
    debug echo "  DEBUG: Exit function remove_img"
}

# check_img()
# mkudffs dump image details?


# MAIN

# "$@" passes input options into get_options function
get_options "$@"

debug printf "  DEBUG: var _image_dir = %s\n" "${_image_dir}"
debug printf "  DEBUG: var _image_name = %s\n" "${_image_name}"
debug printf "  DEBUG: var _rsync_options = %s\n" "${_rsync_options}"
debug printf "  DEBUG: var _mkudffs_lvid = %s\n" "${_mkudffs_lvid}"

echo "Image file: ${_image_dir}/${_image_name}"
echo "Volume label: ${_mkudffs_lvid}"
echo "Copy command: rsync ${_rsync_options} ${_mount_dir}"
echo "Files: ${_files[@]}"

check_vars

check_size

truncate_img

format_img

mount_img

copy_files

unmount_img

echo
echo "File ${_image_dir}/${_image_name} ready."
