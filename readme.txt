mkudffs-img.sh

Usage: ./mkudffs-img.sh -h [help] -l [volume label] -o [image file] -v 
[verbose] ./files

For configuration, edit the variables at the top of mkudffs-img.sh file.  A 
brief explanation follows for what is required for proper usage of this script.

The idealogy for creating this mkudffs-img.sh script, for writing UDF image 
files easier, currently non-existant except for few users knowing how to 
manually create UDF image files.  As of 2023, Microsoft Windows has and 
currently uses a similar process for easy optical media writing.  With the 
exception, this script does not write the final image file to optical media, a 
final minor trivial process for most, hopefully coded with typical Unix 
philosophy.



REQUIREMENTS FOR MKUDFFS.SH
REQUIRED EDITING OF MKUDFFS.SH FILE VARIABLES
HOW TO USE MKUDFFS.SH
DEBUGGING
TIPS



REQUIREMENTS FOR MKUDFFS.SH

Non-bash builtins required for this scripts, likely already provided already by 
your default Linux install.

du - coreutils package name
mkudffs - udftools package name
mount/umount - util-linux, requires sudo! (eg. add access to /etc/sudoers file)
rsync - rsync package name
truncate - coreutils package name



REQUIRED EDITING OF MKUDFFS.SH FILE VARIABLES

_img_dir =
Create a directory having access to a large amount of space, for the maximum 
size of your optical media or image you are creating. Free space required, 
currently 700MB to 50MB for CD-R or BD-R media respecitively.  (eg.  mkdir 
/mnt/tmp2) Ensure you're not using tmpfs (eg. /tmp), else tmpfs free space is 
limited to your amount of hardware memory.

_cdr = 700MB optical media having 737280000 bytes.
_dvd = currently defined for DVD+R/+RW media having maximum 4700372992 bytes.  
DVD-R/DVD-RW will be less.  Maximum byte capacities for most media are noted 
within the mkudffs-img.sh.  Blu-ray media are not plagued by similar sized 
media capacities.  Your physical optical media capacity can be check using 
cdrecord.  Assuming the last line printed by cdrecord is "Remaining writable 
size:", will print your maximum optical media's capacity in bytes:
$ cdrecord -minfo | awk '$1 == "Remaining" {print $4 * 2048}'

_mount_dir =
A simple mount point, for mounting the UDF image file for editing.  (eg. mkdir 
/mnt/tmp2 && chmod a+rwX /mnt/tmp2)



HOW TO USE MKUDFFS.SH

With the previously verified, the mkudffs-img.sh will have sane defaults and 
just work as is.

$ mkudffs-img.sh [some files/folders]

However, maybe wise and provide a label, as all default labels written by 
mkudffs command are LinuxUDF,

$ mkudffs-img.sh -l "my label" [some files/folders]

If writing multiple UDF image files, might specify an image name:
$ mkudffs-img.sh -l "my label" -o my_image.udf [some files/folders]



DEBUGGING

Using "DEBUG=1" method will include debug statements prior to mkudffs-img.sh 
scriptgetopts execution, while "-v" switch will provide debug statements only 
after getopts is executed.

DEBUG=1 mkudffs-img.sh [some files/folders]
mkudffs-img.sh -v [some files/folders]



TIPS

VERIFYING DATA
Mount the optical media and use diff for checking written files.  If the 
optical media has just been written/recorded, first eject and re-insert the 
media for refreshing optical media hardware parameters.
